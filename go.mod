module bitbucket.org/bhargav2020/kraken-go-integrations

go 1.15

require (
	github.com/devopsfaith/krakend v1.2.0
	github.com/xeipuuv/gojsonschema v1.2.0
)
