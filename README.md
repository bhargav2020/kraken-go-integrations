# go-template

Template repository for a go-based project.

```
$ tree
.
├── Makefile
├── README.md
├── build
│   ├── Dockerfile
│   └── docker-entrypoint.sh
├── cmd
│   └── main.go
├── go.mod
├── pkg
│   └── command
│       └── server.go
└── scripts
    ├── check-license.sh
    ├── checks
    └── go-sec.sh
```